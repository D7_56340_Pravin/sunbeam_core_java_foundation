package day4_1;



public class day4_1 
{
	// generic with functions 
	static <T>void genericDisplay(T i)
	{
		System.out.println(i.getClass().getName()+" Value : "+i);
		
	}
	
	public static void main(String[] args) 
	{
		genericDisplay(11);
		genericDisplay("Sunbeam Infotech");
		genericDisplay(5.0);
	}
}
 

/*
//generic
class Test<T,U>
{
	T obj1;
	U obj2;
	
	Test(T obj1,U obj2)
	{	
		this.obj1=obj1;
		this.obj2=obj2;
	}
	
	public void print_data()
	{
		System.out.print("\t"+obj1);
		System.out.println("\t"+obj2);
	}
}



public class day4_1 
{

	public static void main(String[] args) 
	{
		Test<String,Integer> t1=new Test<String,Integer>("Akshita",33);
		Test<Integer,Integer> t2=new Test<Integer,Integer>(50,25);
		Test<Integer,Float> t3=new Test<Integer,Float> (80,80.5f);
		
		t1.print_data();
		t2.print_data();
		t3.print_data();
	}
}

*/

/*
//simple program to demonstrate generics
// <> along with class name
// generic class
class Test<T> 
{
	T obj;
	Test(T obj)
	{
		this.obj=obj;
	}
	public T getObject()
	{
		return this.obj;
	}
}


public class day4_1 
{

	public static void main(String[] args) 
	{
		Test <Integer>t1=new Test<Integer>(15);
		
	//	Test t1=new Test(155); //paramatrized (int) 
		
		System.out.println(t1.getObject());
		
		Test<String> t2 = new Test<String>("Sunbeam");
		System.out.println(t2.getObject());
		Test<Float> t3=new Test<Float>(5.4f);
		System.out.println(t3.getObject());
		
		
	}
}

*/


/*
class Test
{
	int num1;
	Test(int num1) // constructor 
	{
		this.num1=num1;
	}
	void print_data()
	{
		System.out.println("Num1 : "+this.num1);
	}
}
public class day4_1 {

	public static void main(String[] args) 
	{
		Test tobj=new Test(40); //integer data
		tobj.print_data();
		
		//Test t2 = new Test("Akshita"); //not allowed  
		//string type of data 
		
	}

}

*/