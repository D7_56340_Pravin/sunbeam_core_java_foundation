package day4_5;


import java.util.ArrayList;

class Student
{
	int rollno;
	String name;
	int age;
	Student(int rollno, String name,int age)
	{
		this.rollno=rollno;
		this.name=name;
		this.age=age;
	}
}

public class day4_5 
{

	public static void main(String[] args) 
	{
		Student s1=new Student(1,"Akshita",33);
		Student s2=new Student(2,"abc",38);
		Student s3=new Student(3,"pqr",50);
		
		// create ArrayList 
		ArrayList<Student> list=new ArrayList<Student>();
		
		// add objects into ArrayList
		list.add(s1);
		list.add(s2);
		list.add(s3);
		
		System.out.println(list.toString());
		for(int i=0;i<list.size();i++)
		{
			System.out.println(list.get(i));
			System.out.println("RollNo : "+list.get(i).rollno+" Name : "+list.get(i).name+" Age : "+list.get(i).age);
		}

		
		
		
	}

}
