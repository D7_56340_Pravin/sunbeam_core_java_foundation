package day4_6;

import java.util.ArrayDeque;
import java.util.Queue;

public class day4_6
{

	public static void main(String[] args) 
	{
		Queue<Integer> q=new ArrayDeque<Integer>(); // upcasting
		q.add(20);
		q.add(30);
		q.add(50);
		q.add(10);
		
		Integer ele=null;
		while(!q.isEmpty())
		{
			ele=q.element();
			System.out.println("\t"+ele);
			q.remove();
		}
		
	}
}



/*
 * 
import java.util.Stack;

public class day4_6 {

	public static void main(String[] args) 
	{
		Stack<Integer> stk=new Stack<Integer>();
		stk.push(10);
		stk.push(20);
		stk.push(30);
		stk.push(40);
		
		Integer ele=null;
		while(!stk.empty())
		{
			ele=stk.peek();//top element fetched from the stack
			System.out.println("\t"+ele);
			stk.pop();//remove the element at top location
		}
		
		
		
	}

}

*/
