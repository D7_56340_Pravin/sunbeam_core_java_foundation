import java.util.ArrayList;
import java.util.List;



public class day4_4
{
	public static List<Integer> getList() // getList() having a return type as list 
	{
		List<Integer> l1=new ArrayList<>();
		l1.add(10);
		l1.add(20);
		l1.add(30);
		return l1; // function is returning a list of integer data 
		
	}
	public static void main(String args[])
	{
		List<Integer> list=day4_4.getList();
		for(int i=0;i<list.size();i++)
			System.out.print("\t"+list.get(i));
	}
}
