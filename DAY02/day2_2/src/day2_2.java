import java.util.Scanner;

// if we wish to initialize the object 
// Constructor 
// Special Member Function
class Employee
{
	private int id;
	private int salary;
	Scanner sc=new Scanner(System.in);
	
	
	public Employee()
	{
		
	}
	
	public Employee(int id, int salary) {
	
		this.id = id;
		this.salary = salary;
	}
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getSalary() {
		return salary;
	}

	public void setSalary(int salary) {
		this.salary = salary;
	}

	void accept_record() // Employee object address will get stored into this 
	{
		System.out.println("Enter ID :");
		this.id=sc.nextInt();
		System.out.println("Enter Salary :");
		this.salary=sc.nextInt();
	}
	void print_record()
	{
	System.out.println("ID = "+this.id+" Salary = "+this.salary);
	}
}

public class day2_2 {

	public static void main(String[] args) 
	{
		Employee e1=new Employee();
		Employee e2=new Employee(2,70000);
		e1.print_record();
		//e2.print_record();
		System.out.println("Employee E2 ID = "+e2.getId());
		
		Employee e3=new Employee();
		
		
	}
}