package day2_4;

import java.util.Scanner;


public class day2_4 
{
	static Scanner sc=new Scanner(System.in);
	public static void accept_arr(int[][] arr)
	{
		System.out.println("Enter Array Elements :");
		for(int i=0;i<3;i++)
		{
			for(int j=0;j<3;j++)
			{
				arr[i][j]= sc.nextInt();
			}
		}
	}
	public static void disp_arr(int[][] arr)
	{
		System.out.println(" Array Elements are:");
		for(int i=0;i<3;i++)
		{
			for(int j=0;j<3;j++)
			{
				System.out.print("\t"+arr[i][j]);
			}
			System.out.println();
		}
	}
	
	public static void main(String[] args) 
	{
		int a[][]=new int[3][3];
		int b[][]=new int[3][3];
		int c[][]=new int[3][3];
		
		day2_4.accept_arr(a);
		day2_4.accept_arr(b);
		for(int i=0;i<3;i++)
		{
			for(int j=0;j<3;j++)
			{
				c[i][j]=a[i][j]+b[i][j];
			}
			
		}
		day2_4.disp_arr(a);
		day2_4.disp_arr(b);
		day2_4.disp_arr(c);
		
		
		
		
		
	}
	
}


/*
public class day2_4 
{
	static Scanner sc=new Scanner(System.in);
	public static void accept_arr(int[][] arr)
	{
		System.out.println("Enter Array Elements :");
		for(int i=0;i<3;i++)
		{
			for(int j=0;j<3;j++)
			{
				arr[i][j]= sc.nextInt();
			}
		}
	}
	public static void disp_arr(int[][] arr)
	{
		System.out.println(" Array Elements are:");
		for(int i=0;i<3;i++)
		{
			for(int j=0;j<3;j++)
			{
				System.out.print("\t"+arr[i][j]);
			}
			System.out.println();
		}
	}
	
	public static void main(String[] args) 
	{
		int arr[][]=new int[3][3];
		day2_4.accept_arr(arr);
		day2_4.disp_arr(arr);
		
	}
	
}
*/

/*
public class day2_4 
{
	static Scanner sc=new Scanner(System.in);
	public static void accept_arr(int[] arr)
	{
		System.out.println("Enter Array Elements :");
		for(int i=0;i<3;i++)
		{
			arr[i]= sc.nextInt();
			
		}
	}
	public static void disp_arr(int[] arr)
	{
		System.out.println(" Array Elements are:");
		for(int i=0;i<3;i++)
		{
			System.out.println(arr[i]);
			
		}
	}
	
	public static void main(String[] args) 
	{
		int arr[]=new int[3];
		day2_4.accept_arr(arr);
		day2_4.disp_arr(arr);
		
	}
	
}
*/


/*
public class day2_4 {

	public static void main(String[] args) 
	{
		int arr1[]=new int[3]; //valid // int array 
		int arr2[]=null; // invalid
		int [arr2] = null // invalid
		int[] arr3 = new int[5]; //valid 
		
		float arr4[] = new float[7]; //float array 
		
		
		
		

	}

}


*/